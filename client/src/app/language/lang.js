import englishLanguage from "./locale/en_US.json";
import tagalogLanguage from "./locale/tl_PH.json";

// Application's languages
const AppLocale = {
    en: englishLanguage,
    tl: tagalogLanguage,
};

export default AppLocale;
