// Swagger Header table for tag

/**
 * @swagger
 * tags:
 *   name: /analytics
 *   description: Analytics Route
 */

/**
 * @swagger
 * tags:
 *   name: /authentication
 *   description: Authentication Route
 */

/**
 * @swagger
 * tags:
 *   name: /communication
 *   description: Communication Route
 */

/**
 * @swagger
 * tags:
 *   name: /dashboard
 *   description: Dashboard Route
 */

/**
 * @swagger
 * tags:
 *   name: /events
 *   description: Event List Route
 */

/**
 * @swagger
 * tags:
 *   name: /medical-record
 *   description: Medical Record Route
 */

/**
 * @swagger
 * tags:
 *   name: /settings
 *   description: Settings Route
 */

/**
 * @swagger
 * tags:
 *   name: /subscription
 *   description: Subscription Route
 */